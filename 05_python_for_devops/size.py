'''
Lab



Write a script that will show the sizes of all Jupyter notebook files (*.ipynb) in a directory

Remember:

	
glob.glob lets you find all the filenames that match a pattern
	
open(filename).read() gives you a string with the whole contents of the file in it
	
len(s) gives you the length of a string
'''

from glob import glob

ipynb_files = glob('*.ipynb')

for files in ipynb_files:
    with open(files, 'r') as f:
        lines = f.read()
        print(f'The file "{files}" is  {len(lines)} bytes')


