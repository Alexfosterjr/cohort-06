import string

with open('hamlet.txt') as f:
    word_counts = {}
    for line in f:
        words = line.split()
        for item in words:
            exclude = set(string.punctuation)
            word = ''.join(ch.title() for ch in item if ch not in exclude)
            if word in word_counts:
                word_counts[word] += 1
            else:
                word_counts[word] = 1

    for word in sorted(word_counts, key=word_counts.get, reverse=True):
        print(word_counts[word], word)
    