
from calc import calculate
from sys import argv as arg

print(calculate(arg[1], arg[2], arg[3]))

# This is a module
# It lives in the file mymodule.py

def life_universe_everything():
    return 42

def foo():
    print('bar!')
    return 1

public_data = "public stuff!"
# names that begin with _ are considered "private"
_private_data = "private stuff!"

