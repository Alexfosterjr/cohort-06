import sys



def calculate(a, b, operator):
    try:
        a = float(a)
        b= float(b)
        if operator == '+':
            return a + b
        elif operator == '-':
            return a - b
        elif operator == '*':
            return a * b
        elif operator == '/':
            return a / b
        else:
            print('bad operator!', operator)
            return None
    except ZeroDivisionError:
        return 'Can not divied by zero....'

