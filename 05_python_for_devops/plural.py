from sys import argv as arg
import re

def pluralizer(word):
    if re.search("[sxz]$", word):
        return re.sub('$', 'es', word)
    elif re.search("[^aeioudkprgt]h$", word):
        return re.sub('$', 'es', word)
    elif re.search("[^aeiou]y$", word):
        return re.sub( 'y$', 'ies', word  )
    else:
        return re.sub( '$', 's', word  )

for word in arg[1:]:
    print(pluralizer(word))