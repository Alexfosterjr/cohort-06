from prime import is_prime
import pytest

def test_prime_prime_number():
    result = is_prime(2)
    assert result == True

def test_prime_not_prime_number():
    result = is_prime(4)
    assert result == False

def test_prime_not_prime_one():
    result = is_prime(1)
    assert result == False


def test_prime_prime_number_sixty_one():
    result = is_prime(61)
    assert result == True

def test_prime_string():
    result = is_prime('2')
    assert result == True

def test_prime_string():
    with pytest.raises(ValueError):
        result = is_prime('Q')


def test_prime_list():
    with pytest.raises(ValueError):
        result = is_prime([1,2,3])

def test_prime_list():
    with pytest.raises(ValueError):
        result = is_prime('')
