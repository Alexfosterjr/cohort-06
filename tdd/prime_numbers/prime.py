
def is_prime(num):

    try:
        number = int(num)
        if number == 2:
            return True
        if number == 1:
            return False
        
        for digit in range(2, number):
            
            if number % digit == 0:
                return False
            else:
                return True
    except:
        raise ValueError('Please enter an integer')
