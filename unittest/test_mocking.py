from mocking import dbmocking
from unittest.mock import patch

def test_dbmocking_none():
    with patch("os.getenv") as mock_get:
        mock_get.return_value = None
       
        response = dbmocking()
        assert response == None

def test_dbmocking_empty_string():
    with patch("os.getenv") as mock_get:
        mock_get.return_value = ''
       
        response = dbmocking()
        assert response == 'Value Not Returned'

def test_dbmocking_return_hostname():
    with patch("os.getenv") as mock_get:
        mock_get.return_value = 'AlexFoster'
       
        response = dbmocking()
        assert response == 'mysql://AlexFoster'