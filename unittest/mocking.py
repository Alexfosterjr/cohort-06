import os


def dbmocking():
    hostname = os.getenv("DATABASE_HOST")
    
    if hostname == None:
        return None
    elif hostname == '':
        return 'Value Not Returned'
    else:
        return f'mysql://{hostname}'
   
    

print(dbmocking())