def fizzbuzz(num):
    """Returns 'fizz' for multiples of 3,
       'buzz' for multiples of 5,
       'fizzbuzz' for multiples of both,
       or returns the number passed."""
    try:
        result = ''
        if num % 3 == 0:
            result += 'fizz'
        if num % 5 == 0:
            result += 'buzz'
        result = result or num
        return result
    except TypeError:
        return 'Please provide an integer and try again'
    except NameError:
        return 'You used an undefined variable'